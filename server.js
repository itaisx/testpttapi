const fruits = require('./db')
var express = require('express');
var app = express();
const bodyParser = require('body-parser')
const cors = require('cors');

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors());

app.get('/', function (req, res) {
  res.json(fruits);
});
app.get('/:id', (req, res) => {
  res.json(fruits.find(fruit => fruit.id === Number(req.params.id)))
});
app.post('/', function (req, res) {
  console.log(req.body);
  fruits.push(req.body)
  let json = req.body
  res.send(`Add new user '${json}' completed.`)
});


app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});